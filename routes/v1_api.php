<?php

Route::group(
    [
        'middleware' => ['api'],
    ], function () {

    Route::post('/auth/registration', 'V1\AuthController@registration');
    Route::post('/auth/login', 'V1\AuthController@login');

    Route::post('/auth/refresh', 'V1\AuthController@refresh');

    Route::post('/auth/logout', 'V1\AuthController@logout')->middleware('jwt.auth');

    Route::post('/auth/forgot', 'V1\AuthController@forgot');
    Route::post('/auth/reset', 'V1\AuthController@reset');

    Route::get('/profile', 'V1\ProfileController@get')->middleware('jwt.auth');
    Route::post('/profile/update-fcm-token', 'V1\UserFCMTokenController@update')->middleware('jwt.auth');


    Route::get('/incidents', 'V1\IncidentController@index')->middleware('jwt.auth');
    Route::get('/incidents/{incident}', 'V1\IncidentController@show')->middleware('jwt.auth');
    Route::post('/incidents', 'V1\IncidentController@store')->middleware('jwt.auth');

    Route::get('/incidents/{incident}/departure', 'V1\DepartureController@index')->middleware('jwt.auth');
    Route::post('/incidents/{incident}/departure', 'V1\DepartureController@store')->middleware('jwt.auth');
    Route::delete('/departures/{departure}', 'V1\DepartureController@destroy')->middleware('jwt.auth');





});

