<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidents', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->decimal('latitude', 15, 12)->nullable();
            $table->decimal('longitude', 15, 12)->nullable();

            $table->text('address')->nullable();

            $table->boolean('is_solved')->default(false);

            $table->unsignedBigInteger('creator_id')->nullable();

            $table->foreign('creator_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('set null')
                  ->onUpdate('set null');

            $table->longText('report_text')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidents');
    }
}
