<?php

namespace App\Events;

use App\Models\Incident;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class IncidentCreatedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var \App\Models\Incident
     */
    private $incident;


    /**
     * Create a new event instance.
     *
     * @param \App\Models\Incident $incident
     */
    public function __construct(Incident $incident)
    {
        $this->incident = $incident;
    }

}
