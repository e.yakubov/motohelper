<?php


namespace App\Services;


use App\Models\User;
use App\Utils\BearerToken;
use Illuminate\Auth\AuthManager;

class AuthService
{

    /**
     * @var \Tymon\JWTAuth\JWTGuard
     */
    private $auth;

    public function __construct(AuthManager $authManager)
    {
        $this->auth = $authManager->guard('api');
    }

    /**
     * @param \App\Models\User $user
     *
     * @return \App\Utils\BearerToken
     */
    public function login(User $user): BearerToken
    {
        return new BearerToken($this->auth->login($user));
    }

    /**
     *
     */
    public function logout(): void
    {
        $this->auth->logout();
    }

    /**
     * @return \App\Utils\BearerToken
     */
    public function refresh(): BearerToken
    {
        return new BearerToken($this->auth->refresh());
    }


}
