<?php


namespace App\Services;

use App\Events\UserRegistered;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public function register(array $data): User
    {
        $user = User::create([
            'name' => isset($data['name']) ? $data['name'] : '',
            'second_name' => isset($data['second_name']) ? $data['second_name'] : '',
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'address' => $data['address']
        ]);

        UserRegistered::dispatch($user);

        return $user;
    }

    public function findByEmail(string $email): ?User
    {
        return User::whereEmail($email)->first();
    }

    public function validatePassword(User $user, string $password): bool
    {
        return Hash::check($password, $user->password);
    }
}
