<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\IncidentResource;
use App\Models\Incident;
use Illuminate\Http\Request;

class IncidentController extends Controller
{
    public function index()
    {
        return IncidentResource::collection(Incident::all());
    }

    public function show(Incident $incident)
    {
        return IncidentResource::make($incident);
    }


    public function store(Request $request)
    {
        /** @var Incident $incident */
        $incident =  Incident::create($request->all());

        return IncidentResource::make($incident);
    }
}
