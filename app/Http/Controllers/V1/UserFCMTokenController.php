<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserFCMTokenRequest;
use Illuminate\Support\Facades\Response;

class UserFCMTokenController extends Controller
{
    public function update(UserFCMTokenRequest $request)
    {
        /** @var \App\Models\User $user */
        $user               = $request->user();
        $user->device_token = $request->input('device_token');
        $user->save();

        return Response::json([], 200);
    }
}
