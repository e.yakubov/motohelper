<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function get(Request $request)
    {
        $user = $request->user();
        return new UserResource($user);
    }
}
