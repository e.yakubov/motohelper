<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\DepartureResource;
use App\Models\Departure;
use App\Models\Incident;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class DepartureController extends Controller
{

    public function index(Incident $incident)
    {
        $incident->load('departures', 'departures.volunteer');
        return DepartureResource::collection($incident->departures);
    }

    public function store(Incident $incident, Request $request)
    {
        return DepartureResource::make($incident->departures()->create(
            [
                'volunteer_id' => $request->user()->id,
                'status'       => 'on way',
            ]
        ));
    }

    public function destroy(Departure $departure)
    {
        $departure->delete();
        return Response::json([], 200);
    }
}
