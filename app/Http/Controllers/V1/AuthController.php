<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Resources\AuthLoginResource;
use App\Models\PasswordReset;
use App\Models\User;
use App\Notifications\ResetPasswordNotification;
use App\Services\AuthService;
use App\Services\UserService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;


class AuthController extends Controller
{
    /**
     * @var \App\Services\AuthService
     */
    private $auth;
    /**
     * @var \App\Services\UserService
     */
    private $user;

    public function __construct(AuthService $auth, UserService $userService)
    {
        $this->auth = $auth;
        $this->user = $userService;
    }

    public function registration(RegistrationRequest $request)
    {
        $user = $this->user->register($request->validated());

        $token = $this->auth->login($user);

        return AuthLoginResource::make($user)->additional(compact('token'));
    }

    /**
     *
     * @param \App\Http\Requests\LoginRequest $request
     *
     * @return \App\Http\Resources\AuthLoginResource
     * @throws \Throwable
     */
    public function login(LoginRequest $request)
    {
        $user = $this->user->findByEmail($request->email);

        throw_unless($user, new UnauthorizedHttpException('Basic'));

        $token = $this->auth->login($user);

        return AuthLoginResource::make($user)->additional(compact('token'));
    }

    public function forgot(ResetPasswordRequest $request)
    {

        $user = $this->user->findByEmail($request->input('email'));

        if (!$user) {
            return Response::json([], 401);
        }

        $reset = PasswordReset::create([
            'email' => $user->email,
        ]);

        $reset->save();
        $user->notify(new ResetPasswordNotification($reset));
        return Response::json([], 200);
    }

    public function reset(UpdatePasswordRequest $request)
    {
        /** @var User $user */
        $reset = PasswordReset::whereToken($request->input('token'))->first();

        if (!$reset) {
            return Response::json([], 401);
        }

        $user = User::whereEmail($reset->email)->first();

        if (!$user) {
            return Response::json([], 401);
        }

        $user->password = Hash::make($request->input('password'));
        $user->save();

        $reset->delete();

        return Response::json([], 200);

    }

    public function logout()
    {
        $this->auth->logout();
    }

    public function refresh()
    {
        return $this->auth->refresh();
    }

}
