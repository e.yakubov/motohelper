<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DepartureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'volunteer'=> UserResource::make($this->whenLoaded('volunteer')),
            'status'=> $this->status,
            'created_at'=> $this->created_at,
            'incident_id' => IncidentResource::make($this->whenLoaded('incident'))
        ];
    }
}
