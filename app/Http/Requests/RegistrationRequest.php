<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string name
 * @property string email
 * @property string password
 */
class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'string|min:0|nullable',
            'second_name' => 'string|min:0|nullable',
            'email'       => 'required|email|unique:users|max:255',
            'password'    => 'required|string|min:8|max:200',
            'address'     => 'string|min:0|nullable',
        ];
    }
}
