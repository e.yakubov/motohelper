<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Departure
 *
 * @property int $id
 * @property int|null $volunteer_id
 * @property string|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $incident_id
 * @property-read \App\Models\Incident|null $incident
 * @property-read \App\Models\User|null $volunteer
 * @method static Builder|Departure newModelQuery()
 * @method static Builder|Departure newQuery()
 * @method static Builder|Departure query()
 * @method static Builder|Departure whereCreatedAt($value)
 * @method static Builder|Departure whereId($value)
 * @method static Builder|Departure whereIncidentId($value)
 * @method static Builder|Departure whereStatus($value)
 * @method static Builder|Departure whereUpdatedAt($value)
 * @method static Builder|Departure whereVolunteerId($value)
 * @mixin \Eloquent
 */
class Departure extends Model
{

    protected $fillable = [
        'volunteer_id',
        'incident_id',
        'status'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $with = [
        'volunteer'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($departure) {
            $departure->volunteer_id = auth()->user()->id;
        });
    }

    public function incident()
    {
        return $this->belongsTo(Incident::class);
    }

    public function volunteer()
    {
        return $this->belongsTo(User::class, 'volunteer_id', 'id');
    }


}
