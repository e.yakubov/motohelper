<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * App\Models\Incident
 *
 * @property int                                                                               $id
 * @property float|null                                                                        $latitude
 * @property float|null                                                                        $longitude
 * @property string|null                                                                       $address
 * @property int                                                                               $is_solved
 * @property int|null                                                                          $creator_id
 * @property string|null                                                                       $report_text
 * @property \Illuminate\Support\Carbon|null                                                   $created_at
 * @property \Illuminate\Support\Carbon|null                                                   $updated_at
 * @method static Builder|Incident newModelQuery()
 * @method static Builder|Incident newQuery()
 * @method static Builder|Incident query()
 * @method static Builder|Incident whereAddress($value)
 * @method static Builder|Incident whereCreatedAt($value)
 * @method static Builder|Incident whereCreatorId($value)
 * @method static Builder|Incident whereId($value)
 * @method static Builder|Incident whereIsSolved($value)
 * @method static Builder|Incident whereLatitude($value)
 * @method static Builder|Incident whereLongitude($value)
 * @method static Builder|Incident whereReportText($value)
 * @method static Builder|Incident whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\User|null                                                        $creator
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Departure[]             $departures
 * @property-read int|null                                                                     $departures_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null                                                                     $media_count
 */
class Incident extends Model implements HasMedia
{

    use HasMediaTrait;

    protected $fillable = [
        'latitude',
        'longitude',
        'address',
    ];

    protected $casts = [
        'latitude'  => 'double',
        'longitude' => 'double',
        'is_solved' => 'boolean',
    ];


    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $withCount = [
        'departures',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($incident) {
            $incident->creator_id = auth()->user()->id;
        });
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function departures()
    {
        return $this->hasMany(Departure::class);
    }
}
