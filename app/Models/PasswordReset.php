<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Str;

/**
 * App\Models\PasswordReset
 *
 * @property string                          $email
 * @property string                          $token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @method static Builder|PasswordReset newModelQuery()
 * @method static Builder|PasswordReset newQuery()
 * @method static Builder|PasswordReset query()
 * @method static Builder|PasswordReset whereCreatedAt($value)
 * @method static Builder|PasswordReset whereEmail($value)
 * @method static Builder|PasswordReset whereToken($value)
 * @mixin \Eloquent
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|PasswordReset whereId($value)
 * @method static Builder|PasswordReset whereUpdatedAt($value)
 */
class PasswordReset extends Model
{
    protected $fillable = [
        'email',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function (PasswordReset $reset) {
            $reset->generateToken();
        });
    }

    public function generateToken()
    {
        if ($this->token) {
            return;
        }

        $this->token = Str::random(50);
    }
}
