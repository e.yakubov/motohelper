<?php


namespace App\Utils;


use JsonSerializable;

class BearerToken implements JsonSerializable
{
    /**
     * @var string
     */
    private $token;

    public function __construct(string $token)
    {
        $this->token = $token;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function toArray()
    {
        return [
            'access_token' => $this->token,
            'token_type' => 'bearer',
            'expires_in' => $this->expiresIn(),
        ];
    }

    private function expiresIn(): int
    {
        $body = explode('.', $this->token)[1];
        $exp = json_decode(base64_decode($body), true)['exp'];

        return $exp - time();
    }
}
